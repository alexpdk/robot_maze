private val max = 100.0
val slow= 30.52

val NForward = 5
val NShift = 4
val NWheelTurn = 12
val NTurn = 6

val DMin = 3
val DMax = 6

class Navigator(val physics: RobotPhysics){

    enum class Order{
        Back,    //moving back to rotate when close to wall
        Forward, //moving forward by NShift steps to fit passage (not colliding with wall)
        Move,    //basic straight motion by NForward steps (not colliding with wall)
        Ortho,   //attaining position orthogonal to wall
        Swing,   //slight turn to check against tight-to-wall position
        Turn     //turning by 90 degrees
    }

    lateinit var last: Order
    lateinit var callback: ()->Unit

    // rotation is accomplished to left or right direction
    var left = false
    // steps of execution for current order
    var step = 0
    // sensor readings
    var LSen = 0
    var RSen = 0
    // has current order completed
    var next = false

    // adaptive moving backward to reach approximately same sensible distance from the wall
    // TODO change manual value setting to new state BackShift
    var backAdapt = true


    init{ planStraightMotion() }

    private fun attainOrtho():Boolean{

        if(LSen == 0){
            physics.setPower(-slow)
            return false
        }
        step --

        if(LSen < DMin) {
            physics.leftEnginePower.value = -slow*2
            physics.rightEnginePower.value = 0.0
        }else{
            physics.leftEnginePower.value = 0.0
            physics.rightEnginePower.value = slow*2
        }
        if(step < 0) println("Ortho 0 L=$LSen R=$RSen" )
        return step == 0
    }
    fun exec() {
        read()
        when (last) {
            Order.Back -> next = moveBack()
            Order.Forward -> next = move()
            Order.Move -> next = move()
            Order.Ortho -> next = attainOrtho()
            Order.Swing -> next = turn()
            Order.Turn -> next = turn()
            else -> next = true
        }
    }
    fun finish(){
        if (next){
            println("${last.name}")
            callback()
        }
    }
    private fun move():Boolean{
        if(LSen <= DMin || RSen <= DMin){
            physics.setPower(0.0)
            return true
        }else{
            physics.setPower(max)
            step--
            return step==0
        }
    }
    private fun moveBack():Boolean{
        if(!backAdapt)
            physics.setPower(-max)
        else if(LSen <= 1 || RSen <= 1)
            physics.setPower(-slow*3)
        else if(LSen <= DMin || RSen <= DMin)
            physics.setPower(0.0)
        else if(LSen <= 4 || RSen <= 4)
            physics.setPower(slow)
        else
            physics.setPower(slow*2)
        return true
    }
    // robot tried to shift itself when turning to fit passage
    var shifted = false
    private fun planAfterLeftTurn(){
//        println("planAfterLeftTurn")
        backAdapt = true
        // TODO that's required, but call place may be should be changed
        setOrder(Order.Back, {
            read()
            if (LSen == DMax && RSen == DMax) planSwings {
                setOrder(Order.Forward, {
                    setOrder(Order.Ortho, {
                        shifted = false
                        left = false
                        setOrder(Order.Turn, { planStraightMotion() })
                    })
                })
            }
            else if (LSen == DMax) planStep(forward = false, cb = { planAfterLeftTurn() })
            else if (RSen == DMax && !shifted) {
                // only one attempt to shift
                shifted = true

                left = false
                setOrder(Order.Turn, {
                    setOrder(Order.Forward, {
                        left = true
                        setOrder(Order.Turn, { planAfterLeftTurn() })
                    })
                })
            } else {
                shifted = false
                left = false
                setOrder(Order.Turn, { planStraightMotion() })
            }
        })
    }
    fun planAfterRightTurn(){
        //println("planAfterRightTurn")
        read()
        if(LSen == DMax && RSen == DMax) planSwings {planStraightMotion()}
        else planStraightMotion()
    }

    private fun planLeftSwing(cb:()->Unit){
        left = true
        setOrder(Order.Swing, {
            read()
            val tight = LSen < DMax
            left = false
            setOrder(Order.Swing, {
                if(tight){
                    planStep(forward = true, cb={planRightSwing(cb)})
                }
                else planRightSwing(cb)
            })
        })
    }
    private fun planRightSwing(cb:()->Unit){
        left = false
        setOrder(Order.Swing, {
            read()
            val tight = RSen < DMax
            left = true
            setOrder(Order.Swing, {
                if(tight){
                    planStep(forward = false, cb=cb)
                }
                else cb()
            })
        })
    }
    private fun planStep(forward: Boolean, cb: ()->Unit){
        left=forward
        setOrder(Order.Turn, {
            backAdapt = false
            setOrder(Order.Back, {
                left = !forward
                setOrder(Order.Turn, cb)
            })
        })
    }
    private fun planStraightMotion(){
//        println("planStraightMotion")
        setOrder(Order.Move, {
            read()
            if(LSen > DMin && RSen > DMin || step < 2){
                left = true
                setOrder(Order.Turn, {planAfterLeftTurn() })
            }
            else{
                backAdapt = true
                setOrder(Order.Back, {
                    left = false
                    setOrder(Order.Turn, { planAfterRightTurn() })
                })
            }
        })
    }
    private fun planSwings(cb: () -> Unit) = planLeftSwing(cb)

    private fun read(){
        LSen = physics.leftSensorReadings.value
        RSen = physics.rightSensorReadings.value
    }
    fun resetPlan(){
        next = false
        planStraightMotion()
    }
    fun setOrder(next: Order, cb:()->Unit){
        step = when(next) {
            Order.Forward -> NShift
            Order.Move -> NForward
            Order.Ortho -> NWheelTurn
            Order.Swing -> 1
            Order.Turn -> NTurn
            else -> 0
        }
        last = next
        callback = cb
    }
    private fun turn():Boolean{
        if(left && LSen <= 1 || !left && RSen <= 1){
            physics.setPower(-slow)
            return false
        }else {
            physics.applyTurn(left, power = slow)
            step--
            return step == 0
        }
    }
}
