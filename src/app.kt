import javafx.scene.input.KeyCode

class Main : javafx.application.Application(){

    override fun start(primaryStage: javafx.stage.Stage) {
        val fxmlLoader = javafx.fxml.FXMLLoader(Main::class.java.getResource("scene.fxml"))
        val parent : javafx.scene.Parent = fxmlLoader.load()

        val c : Controller = fxmlLoader.getController()
        c.initStageDependent(primaryStage)

        primaryStage.title = "Robot_Maze"
        primaryStage.scene = javafx.scene.Scene(parent)

        val scene = primaryStage.scene

        primaryStage.show()

        scene.setOnKeyPressed { e ->
            when(e.code){
                KeyCode.EQUALS -> /*if(e.isShiftDown)*/ c.incScale()
                KeyCode.MINUS -> c.decScale()
                KeyCode.A -> c.translate(-1, 0)
                KeyCode.D -> c.translate(1, 0)
                KeyCode.W -> c.translate(0, -1)
                KeyCode.S -> c.translate(0, 1)
            else -> Unit
        }}
        c.setTimer()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Main::class.java)
        }
    }
}