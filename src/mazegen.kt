import java.io.*
import java.util.*

val X = arrayOf(0, 1, 0, -1)
val Y = arrayOf(1, 0, -1, 0)

class MazeGenerator(private val width: Int, private val height: Int, private val cellSize: Int){

    private fun inMaze(x: Int, y:Int) = x in 0..width-1 && y in 0..height-1

    private inline fun knightPair(x: Int, y:Int, i:Int, j:Int, check:(x:Int,y:Int)->Boolean) =
        if(i==0) check(x-1,y+j) && check(x+1,y+j) else check(x+i,y-1) && check(x+i,y+1)

    inline fun forNei(x0:Int, y0:Int, act: (Int, Int)->Unit){
        for(i in X.indices) act(x0+X[i],y0+Y[i])
    }

    private data class Passage(val x:Int, val y:Int, val i:Int, val j:Int)

    fun generate() : Array<IntArray>{
        val Wall = 1
        var Mark = 2
        val matrix = Array(width, {IntArray(height, {Wall})})
        val neiNum = Array(width, {IntArray(height)})

        val rand = Random(System.nanoTime())
        val pass = arrayListOf(Passage(0,0,1,0))
        neiNum[0][0]=1
        while (pass.size > 0) {
            val p = pass.removeAt(rand.nextInt(pass.size))
            if (neiNum[p.x][p.y] == 1 && knightPair(p.x, p.y, p.i, p.j, { x, y -> !inMaze(x, y) || matrix[x][y] == Wall })) {
                matrix[p.x][p.y] = 0
                forNei(p.x, p.y, { x, y ->
                    if (inMaze(x, y) && matrix[x][y] == Wall) {
                        if (neiNum[x][y] == 0) pass.add(Passage(x, y, x - p.x, y - p.y))
                        neiNum[x][y]++
                    }
                })
            }
        }
        var x = rand.nextInt(matrix.size)
        var y = rand.nextInt(matrix[0].size)
        while(matrix[x][y] == Wall){
            x = rand.nextInt(matrix.size)
            y = rand.nextInt(matrix[0].size)
        }

        val points = Array(width*cellSize, {IntArray(height*cellSize, {Wall})})
        for(i in 0 until width) for (j in 0 until height){
            if(i == x && j == y){
                for(k in i*cellSize until (i+1)*cellSize) for(l in j*cellSize until (j+1)*cellSize)
                    points[k][l] = if(Math.hypot(
                        k - (i+0.5)*cellSize, l - (j+0.5)*cellSize
                    ) <= cellSize / 3) Mark else 0
            }
            else for(k in i*cellSize until (i+1)*cellSize) for(l in j*cellSize until (j+1)*cellSize)
                 points[k][l] = matrix[i][j]
        }
        return points
    }
    fun loadMaze(path: String): Array<IntArray>{
        val file = File(path)
        val points = ArrayList<IntArray>()

        BufferedReader(FileReader(file)).use({ br ->
            var line: String? = br.readLine() // skip width and height

            while (br.let { line=it.readLine(); line != null }) {
                val args = line!!.split(' ').filter { it.length > 0 }.map { it.toInt() }.toIntArray()
                points.add(args)
            }
        })
        return points.toTypedArray()
    }
    fun saveMaze(path: String, points: Array<IntArray>){
        val file = File(path)
        val fop = FileOutputStream(file)

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStreamWriter(fop).use {
            it.write("${width*cellSize} ${height*cellSize}")
            for(line in points){
                it.write("\n")
                for(point in line) it.write("$point ")
            }
        }
    }
}

/* Some test values for robot
        matrix[0][1] = 0
        matrix[1][0] = 0
        matrix[0][3] = 0
        matrix[0][4] = 1
        matrix[1][4] = 0
        matrix[1][3] = 0
        matrix[2][3] = 0
        matrix[1][2] = 0
        matrix[1][1] = 0
        matrix[1][5] = 0
        matrix[2][4] = 1
        matrix[0][5] = 0
        matrix[1][6] = 1
        matrix[0][2] = 1*/