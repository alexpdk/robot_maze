
import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Point2D
import javafx.scene.SnapshotParameters
import javafx.scene.canvas.GraphicsContext
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.paint.Color

fun Double.approx(other: Double, appr:Double=0.001) : Boolean =
        Math.abs(this-other)<appr

class Robot(startX: Double, startY: Double, private val check: (x: Double, y: Double, i: Image) -> Boolean,
        private val see: (x: Double, y:Double, rad:Double) -> Int){

    private val image = Image("file:./res/robo2x.png")
    private val imView = ImageView(image)
    private var rotated = image
    private val snapParams = SnapshotParameters()

    // robot wheel axis center
    var centerX = CX+startX
    var centerY = CY+startY

    val isStuck = SimpleBooleanProperty(false)

    var rotation = 0.0
        get() = field
        set(value){
            field = value
            imView.rotate = value
            rotated = imView.snapshot(snapParams, null)
        }
    private var gCenter = getGeomCenter()

    data class IntPair(val a:Int, val b:Int)

    // TODO in production
    private fun isPositionValid(x: Double, y: Double, i: Image) : Boolean{
        // should accept  maze position of sprite geometrical center
        assert(!x.approx(centerX) || !y.approx(centerY),
                {"isPositionValid accepts position of geometrical center!"})
        return check(x,y,i)
    }

    init {
        println("image width=${image.width} height=${image.height}")
        snapParams.fill = Color.TRANSPARENT
    }

    fun draw(ctx: GraphicsContext){
        ctx.save()
        val image = rotated
        val ww = image.width/2
        val hh = image.height/2
        ctx.translate(gCenter.x, gCenter.y)
        ctx.drawImage(image,-ww,-hh,image.width,image.height)
        ctx.restore()
    }

    private fun getGeomCenter(rad: Double = Math.toRadians(rotation)):Point2D{
        return Point2D(centerX+Math.cos(rad)*DIST, centerY+Math.sin(rad)*DIST)
    }
    private fun getLeftSensor(rad: Double):Point2D{
        return Point2D(centerX+Math.cos(rad-SENSOR_ANG)*SENSOR_DIST, centerY+Math.sin(rad-SENSOR_ANG)* SENSOR_DIST)
    }
    private fun getRightSensor(rad: Double):Point2D{
        return Point2D(centerX+Math.cos(rad+SENSOR_ANG)*SENSOR_DIST, centerY+Math.sin(rad+SENSOR_ANG)* SENSOR_DIST)
    }

    fun move(dist: Double = maxStep): Boolean{
        var moved = true
        val sign = Math.signum(dist)
        var rest = dist*sign
        val rad = Math.toRadians(rotation)

        while(rest > 0 && moved) {
            val delta = Math.min(maxStep, rest) * sign
            val dX = delta * Math.cos(rad)
            val dY = delta * Math.sin(rad)
            if (isPositionValid(gCenter.x + dX, gCenter.y + dY, rotated)) {
                centerX += dX
                centerY += dY
                gCenter = getGeomCenter(rad)
                moved = true
            }
            else moved = false
            rest -= delta*sign
        }
        isStuck.value = !moved
        return moved
    }

    fun resetPosition(startX: Double, startY: Double){
        centerX = CX+startX
        centerY = CY+startY
        rotation = 0.0
        gCenter = getGeomCenter()
    }

    fun rotate(angle: Double = maxAngle): Boolean{
        val sign = Math.signum(angle)
        var rest = angle*sign
        var fit = true
        while(rest > 0 && fit) {
            val delta = Math.min(maxAngle, rest)*sign
            rotation += delta
            gCenter = getGeomCenter()
            if( !isPositionValid(gCenter.x, gCenter.y, rotated)) {
                rotation -= delta
                gCenter = getGeomCenter()
                fit = false
            }
            else fit = true
            rest -= delta*sign
        }
        isStuck.value = !fit
        return fit
    }

    fun turnAroundWheel(left: Boolean, angle: Double = maxAngle): Boolean{
        val sign = Math.signum(angle)
        var rest = angle*sign
        var moved = true

        while(rest > 0 && moved){
            val wheelSign = if(left) 1 else -1
            val delta = Math.min(maxAngle, rest)*wheelSign*sign
            val rad = Math.toRadians(rotation - 90.0)
            val dRad = Math.toRadians(delta/2)

            val dT = 2*WHEEL_DIST*Math.sin(dRad)*wheelSign
            val dX = -dT*Math.sin(dRad+rad)
            val dY = dT*Math.cos(dRad+rad)

            centerX += dX
            centerY += dY
            rotation += delta
            gCenter = getGeomCenter()
            if( !isPositionValid(gCenter.x, gCenter.y, rotated)){
                centerX -= dX
                centerY -= dY
                rotation -= delta
                gCenter = getGeomCenter()
                moved = false
            }
            else moved = true
            rest -= delta*wheelSign*sign
        }
        isStuck.value = !moved
        return moved
    }

    fun view():IntPair{
        val rad = Math.toRadians(rotation)
        val left = getLeftSensor(rad)
        val right = getRightSensor(rad)
        return IntPair(see(left.x,left.y,rad), see(right.x, right.y, rad))
    }

    companion object{
        const val maxStep = 3.0  // px
        const val maxAngle = 5.0 // degrees
        // Position of rotation center
        const val CX = 3.0
        const val CY = 10.0

        val SENSOR_ANG = Math.atan2(10.0, 13.0)
        val SENSOR_DIST = Math.hypot(10.0, 13.0)

        /** Distance between geometrical and rotation centers */
        const val DIST = 5.0
        /** Distance between rotation center and wheel center */
        const val WHEEL_DIST = 7.0
    }
}