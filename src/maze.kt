import javafx.scene.control.Alert
import javafx.scene.image.Image
import javafx.scene.image.WritableImage
import javafx.scene.paint.Color

private const val max = 255.0      // White color(255, 255, 255)
private const val PxScale = 2      // Number of pixels per arbitrary unit

class Maze(matrix : Array<IntArray>){

    var image = WritableImage(matrix.size*PxScale, matrix[0].size*PxScale)

    private var reader = image.pixelReader
    private var writer = image.pixelWriter

    private val floorColor = Color.color(49/max,147/max,111/max)
    private val wallColor = Color.color(51/max,40/max,20/max)

    var markReached = false

    init { setMatrix(matrix) }

    private fun closeInt(v:Double, mark:Double):Int =
            if(mark>0) Math.ceil(v).toInt() else Math.floor(v).toInt()

    fun countEmptyDots(sensorX: Double, sensorY: Double, rotRadian: Double): Int{
        val sin = Math.sin(rotRadian)
        val cos = Math.cos(rotRadian)

        val x0 = closeInt(sensorX, cos)
        val y0 = closeInt(sensorY, sin)
        if(x0 < 0 || x0 >=image.width || y0 < 0 || y0 >= image.height ||
            reader.getColor(x0, y0)==wallColor) return 0

        for(i in 1..6){
            val x = closeInt(sensorX+i*PxScale*cos, cos)
            val y = closeInt(sensorY+i*PxScale*sin, sin)
            if(x < 0 || x >=image.width || y < 0 || y >= image.height ||
            reader.getColor(x, y)==wallColor) return i-1
        }
        return 6
    }

    private fun drawCell(x: Int, y: Int, color: Color){
        for(i in x*PxScale until (x+1)*PxScale) {
            for (j in y * PxScale until (y + 1) * PxScale) {
                writer.setColor(i, j, color)
            }
        }
    }
    // accepts sprite and position of its center in the maze

    fun hasSpaceForSprite(centerX: Double, centerY: Double,  sprite: Image): Boolean{
        val minX = Math.floor(centerX-sprite.width/2).toInt()
        val maxX = Math.ceil(centerX+sprite.width/2).toInt()
        val minY = Math.floor(centerY-sprite.height/2).toInt()
        val maxY = Math.ceil(centerY+sprite.height/2).toInt()
        //println("a w=$centerX h=$centerY $minX $minY $maxX $maxY")
        if(minX < 0 || maxX > image.width || minY < 0 || maxY > image.height) return false

        var inter = 0

        val sReader = sprite.pixelReader
        for(i in 0 until sprite.width.toInt()) for(j in 0 until sprite.height.toInt()){
            if(sReader.getColor(i, j) != Color.TRANSPARENT){
                val color = reader.getColor(i+minX,j+minY)
                if(color == wallColor) inter++
                else if(color == Color.BLACK) markReached=true
            }
        }
        return inter < 3
    }

    fun setMatrix(matrix : Array<IntArray>){
        print("D: ${image.width}, ${image.height}, ${matrix[0].size}, ${matrix.size}")

        image = WritableImage(matrix.size*PxScale, matrix[0].size*PxScale)
        reader = image.pixelReader
        writer = image.pixelWriter

        for(i in matrix.indices) for(j in matrix[0].indices){
            drawCell(i, j, when(matrix[i][j]){
                0 -> floorColor
                1 -> wallColor
                else -> Color.BLACK
            })
        }
        markReached = false
    }
}