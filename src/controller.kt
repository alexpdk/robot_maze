import javafx.animation.AnimationTimer
import javafx.beans.binding.Bindings
import javafx.fxml.FXML
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.io.File

class Controller{
    @FXML lateinit var canvas: Canvas
    @FXML lateinit var generate: Button
    @FXML lateinit var link: ToggleButton
    @FXML lateinit var linkPower: ToggleButton
    @FXML lateinit var load: Button
    @FXML lateinit var reset: Button
    @FXML lateinit var save: Button
    @FXML lateinit var stopNav: Button

    @FXML lateinit var leftB: Button
    @FXML lateinit var leftF: Button
    @FXML lateinit var lSensor: Label
    @FXML lateinit var lSlider: Slider
    @FXML lateinit var lPower: Label
    @FXML lateinit var rightB: Button
    @FXML lateinit var rightF: Button
    @FXML lateinit var rSensor: Label
    @FXML lateinit var rSlider: Slider
    @FXML lateinit var rPower: Label
    @FXML lateinit var stuckLight: ImageView

    @FXML lateinit var xPos: Label
    @FXML lateinit var yPos: Label
    @FXML lateinit var angle: Label

    private lateinit var ctx: GraphicsContext
    private lateinit var gen: MazeGenerator
    private lateinit var matrix: Array<IntArray>
    private lateinit var maze: Maze
    private lateinit var navigator: Navigator
    private lateinit var robot: Robot
    private lateinit var robotPhysics: RobotPhysics
    private var navRunning = false
    private var timer: AnimationTimer? = null
    private lateinit var view: ViewField

    private val lightOn = Image("file:./res/light-on.png")
    private val lightOff = Image("file:./res/light-off.png")

    fun decScale(){
        view.scale*=0.91
    }
    fun disableManual(){
        leftF.disableProperty().value = true
        leftB.disableProperty().value = true
        rightF.disableProperty().value = true
        rightB.disableProperty().value = true
        link.disableProperty().value = true
        linkPower.disableProperty().value = true
        lSlider.disableProperty().value = true
        rSlider.disableProperty().value = true

        stopNav.disableProperty().value = false

        robotPhysics.leftEngineDir.value = 1
        robotPhysics.rightEngineDir.value = 1

        Bindings.unbindBidirectional(robotPhysics.leftEngineDir, robotPhysics.rightEngineDir)
        link.selectedProperty().value = false
    }
    fun draw(){
        ctx.save()
        ctx.clearRect(0.0,0.0,canvas.width,canvas.height)
        view.centerRobot(robot)
        view.draw()
        robot.draw(ctx)
        ctx.restore()
    }
    fun enableManual(){
        leftF.disableProperty().value = false
        leftB.disableProperty().value = false
        rightF.disableProperty().value = false
        rightB.disableProperty().value = false
        link.disableProperty().value = false
        linkPower.disableProperty().value = false
        lSlider.disableProperty().value = false
        rSlider.disableProperty().value = false

        stopNav.disableProperty().value = true

        robotPhysics.leftEngineDir.value = 0
        robotPhysics.rightEngineDir.value = 0
    }
    fun incScale(){
        view.scale*=1.1
    }
    fun initialize() {
        ctx = canvas.graphicsContext2D
        gen = MazeGenerator(width = 30, height = 30, cellSize = 20)

        matrix = gen.generate()
        maze = Maze(matrix)
        view = ViewField(canvas, maze)

        // robot accepts method to validate its position
        robot = Robot(3.0, 9.0,
                {x,y,sprite->maze.hasSpaceForSprite(x,y,sprite)}, // is position valid
                {x,y,rad->maze.countEmptyDots(x,y,rad)})          // see free dots by sensor
        robotPhysics = RobotPhysics(robot)
        navigator = Navigator(robotPhysics)

        robotPhysics.leftSensorReadings.addListener { src, old, new ->
            lSensor.text = "Left sensor see $new free dots"
        }
        robotPhysics.rightSensorReadings.addListener { src, old, new ->
            rSensor.text = "Right sensor see $new free dots"
        }

        lSlider.valueProperty().addListener { src, old, new ->
            lSlider.value = new.toInt().toDouble()
            lPower.text="Left engine power: ${new.toInt()}"}

        rSlider.valueProperty().addListener { src, old, new ->
            rSlider.value = new.toInt().toDouble()
            rPower.text="Right engine power: ${new.toInt()}"}

        Bindings.bindBidirectional(lSlider.valueProperty(), robotPhysics.leftEnginePower)
        Bindings.bindBidirectional(rSlider.valueProperty(), robotPhysics.rightEnginePower)

        disableManual()

        leftF.pressedProperty().addListener { src,old,new->
            robotPhysics.leftEngineDir.value = if(new) 1 else 0
        }
        leftB.pressedProperty().addListener { src,old,new->
            robotPhysics.leftEngineDir.value = if(new) -1 else 0
        }
        rightF.pressedProperty().addListener { src,old,new->
            robotPhysics.rightEngineDir.value = if(new) 1 else 0
        }
        rightB.pressedProperty().addListener { src,old,new->
            robotPhysics.rightEngineDir.value = if(new) -1 else 0
        }

        link.selectedProperty().addListener { src,old,new->
            if(new)
                Bindings.bindBidirectional(robotPhysics.leftEngineDir, robotPhysics.rightEngineDir)
            else
                Bindings.unbindBidirectional(robotPhysics.leftEngineDir, robotPhysics.rightEngineDir)
        }

        linkPower.selectedProperty().addListener { src, old, new->
            if(new)
                Bindings.bindBidirectional(robotPhysics.leftEnginePower, robotPhysics.rightEnginePower)
            else
                Bindings.unbindBidirectional(robotPhysics.leftEnginePower, robotPhysics.rightEnginePower)
        }

        stuckLight.image = lightOff
        robot.isStuck.addListener { src, old, new ->
            //println(robot.isStuck.value)
            if (new) print("STUCK ")
            stuckLight.image = if (new == true) lightOn else lightOff
        }
        stopNav.pressedProperty().addListener { src,old,new-> if(new) if(navRunning){
            navRunning=false
            enableManual()
        }else{
            navRunning = true
            stopNav.text = "Stop"
        }}
    }
    fun initStageDependent(stage: Stage){
        val fileChooser = FileChooser()
        fileChooser.initialDirectory = File("res")
        fileChooser.extensionFilters.add(FileChooser.ExtensionFilter("Text files","*.txt"))

        generate.pressedProperty().addListener { src,old,new-> if(new) reloadMaze(gen.generate()) }

        load.pressedProperty().addListener { src,old,new->if(new){
            fileChooser.title = "Open Maze"
            val file = fileChooser.showOpenDialog(stage)
            if(file != null){
                reloadMaze(gen.loadMaze(file.path))
            }
        }}

        reset.pressedProperty().addListener { src,old,new->if(new) reloadMaze(matrix) }

        save.pressedProperty().addListener { src,old,new-> if(new){
            fileChooser.title = "Save Maze"
            val file = fileChooser.showSaveDialog(stage)
            if (file != null) {
                gen.saveMaze(file.path, matrix)
            }
        }}
    }
    fun reloadMaze(_matrix: Array<IntArray>){
        matrix = _matrix
        maze.setMatrix(matrix)
        robot.resetPosition(3.0, 9.0)
        robotPhysics.readSensors()
        robotPhysics.setPower(0.0)
        navigator.resetPlan()

        navRunning = false
        stopNav.text = "Start"

        disableManual()
        setTimer()
    }
    fun setTimer(){
        var frameCount = 0
        if(timer == null) {
            timer = object : AnimationTimer() {
                override fun handle(now: Long) {
                    if (frameCount++ % 1 == 0) update()
                    draw()
                }
            }
            timer?.start()
        }
    }
    fun stopTimer(){
        timer?.stop()
        timer = null
    }
    fun translate (dX:Int, dY:Int){
        view.translate(dX*10.0, dY*10.0)
    }
    fun update(){
        if (navRunning) navigator.exec()
        robotPhysics.moveRobot()
        robotPhysics.readSensors()
        if (navRunning) navigator.finish()
        if(maze.markReached){
            stopTimer()
            Alert(Alert.AlertType.INFORMATION, "Robot reached mark").show()
        }
        xPos.text = "X: ${robot.centerX.toInt()}"
        yPos.text = "Y: ${maze.image.height - robot.centerY.toInt()}"
        angle.text = "A: ${robot.rotation.toInt()}"
    }
}
