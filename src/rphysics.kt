import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty

const val POWER_2_TRAN = Robot.maxStep / 100

// step * TRAN_2_WHEEL_ROT = rotation angle
const val TRAN_2_WHEEL_ROT = 180.0 / (Robot.WHEEL_DIST*2*Math.PI)

const val POWER_2_WHEEL_ROT = POWER_2_TRAN * TRAN_2_WHEEL_ROT

const val POWER_2_ROT = POWER_2_WHEEL_ROT*2

class RobotPhysics(val robot: Robot){

    val leftEngineDir = SimpleIntegerProperty(1)
    val leftEnginePower = SimpleDoubleProperty(0.0)
    var leftSensorReadings = SimpleIntegerProperty(6)

    val rightEngineDir = SimpleIntegerProperty(1)
    val rightEnginePower = SimpleDoubleProperty(0.0)
    var rightSensorReadings = SimpleIntegerProperty(6)

    fun applyTurn(left: Boolean, power: Double){
        val lSign = if(left) -1 else 1
        leftEnginePower.value = lSign*power
        rightEnginePower.value = -lSign*power
    }

    fun moveRobot() {
        val lep = if (leftEngineDir.value != 0) Math.abs(leftEnginePower.value) else 0.0
        val rep = if (rightEngineDir.value != 0) Math.abs(rightEnginePower.value) else 0.0
        val lSign = Math.signum(leftEnginePower.value) * leftEngineDir.value
        val rSign = Math.signum(rightEnginePower.value) * rightEngineDir.value

        val common = Math.min(lep, rep)
        val left = lep >= rep
        val remPower = Math.max(lep, rep) - common

        if (lSign == rSign) {
            robot.move(2 * common * lSign * POWER_2_TRAN)
            if (!remPower.approx(0.0)) {
                robot.turnAroundWheel(left, remPower * lSign * POWER_2_WHEEL_ROT)
            }
        } else {
            robot.rotate(2 * common * lSign * POWER_2_ROT)
            if (!remPower.approx(0.0)) {
                // required, because left/right wheels have opposite directions
                val sign = if (left) lSign else rSign
                robot.turnAroundWheel(left, remPower * sign * POWER_2_WHEEL_ROT)
            }
        }
        /*if(robot.isStuck.value) {
            print("S ")
        }*/
    }
    fun readSensors(){
        val (left, right) = robot.view()
        leftSensorReadings.value = left
        rightSensorReadings.value = right
    }
    fun setPower(power: Double){
        leftEnginePower.value = power
        rightEnginePower.value = power
    }
}