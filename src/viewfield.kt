import javafx.scene.canvas.Canvas

class ViewField(val canvas: Canvas, val maze: Maze){
    private val ctx = canvas.graphicsContext2D

    var scale=1.0
    private var transX = 0.0
    private var transY = 0.0

    fun centerRobot(robot: Robot) {
        transX = clamp(robot.centerX - canvas.width / 2 / scale, 0.0, maze.image.width - canvas.width / scale)
        //transY = -clamp(robot.centerY - canvas.height/2/scale, 0.0, maze.image.height - canvas.height/scale)
        transY = clamp(robot.centerY + canvas.height / 2 / scale, canvas.height / scale, maze.image.height)
    }

    private fun clamp(v: Double, min: Double, max: Double) =
        if(v < min || max < min) min else if(v > max) max else v

    fun draw() {
        ctx.translate(-transX*scale, transY*scale)
        ctx.scale(scale, -scale)
        ctx.drawImage(maze.image, 0.0, 0.0)
    }
    fun translate(dX: Double, dY: Double){
        transX += dX
        transY += dY
    }
}